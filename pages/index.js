import Header from '../components/Header';
import Image from 'next/image';
import { useState } from 'react';

const apiKey = '38fbe3da0f14af9899a406269d7463ea';


export const getStaticProps = async () => {
  const movies = await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}`)
  .then((res) => res.json())
  .then((data) => data.results);

  const genres = await fetch(`https://api.themoviedb.org/3/genre/movie/list?api_key=${apiKey}`)
  .then((res) => res.json())
  .then((data) => data.genres);

  return {
    props: {
      movies,
      genres,
    }
  }
}

const Home = ({ movies, genres }) => {
  const [currentMovie, setCurrentMovie] = useState({});

  let fetchMovie = async (movieId) => {
    await fetch(`https://api.themoviedb.org/3/movie/${movieId}?api_key=${apiKey}`)
    .then((res) => res.json())
    .then((res) => {
      setCurrentMovie(res)
    })
  }

  const handleClick = (movieId = null) => {
    if(movieId) {
      fetchMovie(movieId);
    } else {
      setCurrentMovie({});
    }
  }
  
  
  const getGenre = (genreId) => { 
    return genres.sort((a, b) => a.id > b.id).reverse().find((genre) => genre.id <= genreId).name;
  }

  const getTimeFormat = (runtime) => {
    return `${Math.floor(runtime / 60)} h ${runtime % 60}`;
  }

  const getYear = (date) => {
    return date.split('-')[0];
  } 

  return (
    <>
    <Header />
    <main className="home">
      <h1 className="home__title">Popular Movies</h1>
      <section className="home__content">
      <ul className={currentMovie.id ? "home__movies home__movies--reduce" : "home__movies"}>
        {movies.map((movie, index) => (
          <li key={index} onClick={() => handleClick(movie.id)} className="home__movie">
            <figure className="movie">
              <span className="movie__rate">
                <svg className="rate__icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                  <path d="M12 5.173l2.335 4.817 5.305.732-3.861 3.71.942 5.27-4.721-2.524-4.721 2.525.942-5.27-3.861-3.71 5.305-.733 2.335-4.817zm0-4.586l-3.668 7.568-8.332 1.151 6.064 5.828-1.48 8.279 7.416-3.967 7.416 3.966-1.48-8.279 6.064-5.827-8.332-1.15-3.668-7.569z"/>
                </svg>
                <p className="rate__score">{movie.vote_average}</p>
              </span>
              <div className="movie__poster">
                <Image 
                  src={`http://image.tmdb.org/t/p/w500${movie.poster_path}`}
                  alt={`Poster of the movie ${movie.title}`}
                  layout='fill'
                />
                
              </div>
              <figcaption className="movie__description">
                  <h3 className="description__title">{movie.title}</h3>
                  <p className="description__genre">
                    {movie.genre_ids.map((currentGenre) => (
                      getGenre(currentGenre)
                    )).join(' · ')}
                  </p>
                </figcaption>
            </figure>
          </li>
        ))}
      </ul>
      <figure className={currentMovie.id ? "home__details home__details--toggle" : "home__details"}>
        <div className="details__illustration">
            <Image 
              src={`http://image.tmdb.org/t/p/w1280${currentMovie.backdrop_path}`}
              alt={`Poster of the movie ${currentMovie.title}`}
              objectFit='cover'
              layout='fill'
            />
        </div>
        <figcaption className="details__description">
          <svg onClick={() => handleClick()} className="description__button" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" width="48px" height="48px"><path d="M 4.7070312 3.2929688 L 3.2929688 4.7070312 L 10.585938 12 L 3.2929688 19.292969 L 4.7070312 20.707031 L 12 13.414062 L 19.292969 20.707031 L 20.707031 19.292969 L 13.414062 12 L 20.707031 4.7070312 L 19.292969 3.2929688 L 12 10.585938 L 4.7070312 3.2929688 z"/></svg>          <h3 className="description__title">{currentMovie.title}</h3>
          <span className="description__informations">
            {`${currentMovie.id ? getYear(currentMovie.release_date) : ''} · `} 
            {`${currentMovie.id ? currentMovie.genres.map((currentGenre) => (
              currentGenre.name
            )).join(' · ') : ''} · `}
            {getTimeFormat(currentMovie.runtime)} 
          </span>
          <p className="description__resume">{currentMovie.overview}</p>
        </figcaption>
      </figure>
      </section>
    </main>
    </>
  )
}




export default Home;